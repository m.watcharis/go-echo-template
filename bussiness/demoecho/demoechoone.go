package demoecho

import "fmt"

type Demouser interface {
	Getuser()
}

type User struct {
	Fname  string `json:"fname"`
	Lname  string `json:"lanme"`
	Detail struct {
		Email string `json:"email"`
		Phone string `json:"phone"`
	}
}

type DETAIL map[string]interface{}

func (u *User) Showdemouser() string {
	fmt.Println("u ======>", u)
	return "demo user succecss"
}

func (u *User) Handleuser() {
	u.Fname = "testuser"
	u.Lname = "testuserlname"
	u.Detail.Email = "test@test.com"
	u.Detail.Phone = "0154567894"
}

func Processuser() string {
	u := &User{}
	u.Handleuser()
	result := u.Showdemouser()
	fmt.Println("result ======>", result)
	return "Processuser ok"
}

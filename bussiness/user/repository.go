package user

import (
	"watcharis/model"
)

type Repository interface {
	AddUser(data model.UserRequest) (res string, err error)
	Login(data model.UserLogin) (res model.UserLogin, err error)
}

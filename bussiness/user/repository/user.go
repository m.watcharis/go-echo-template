package repository

import (
	"database/sql"
	"errors"
	"fmt"
	"watcharis/bussiness/user"
	"watcharis/model"
)

type mysqlUserRepository struct {
	Conn *sql.DB
}

func NewUserRepository(Conn *sql.DB) user.Repository {
	return &mysqlUserRepository{
		Conn: Conn,
	}
}

func (m *mysqlUserRepository) AddUser(data model.UserRequest) (res string, err error) {
	query := `INSERT INTO users(firstname, lastname, email) VALUES(?, ?, ?)`
	insert, err := m.Conn.Prepare(query)
	if err != nil {
		return "prepare fail", handleErrorMysql(err)
	}

	_, err = insert.Exec(data.Firstname, data.Lastname, data.Email)
	if err != nil {
		return "insert fail", handleErrorMysql(err)
	}
	defer insert.Close()
	return "AddUser success", nil
}

func (m *mysqlUserRepository) Login(data model.UserLogin) (res model.UserLogin, err error) {
	query := `SELECT users.firstname, users.email FROM users WHERE firstname=? AND email=?`
	result, err := m.Conn.Query(query, data.Firstname, data.Email)
	if err != nil {
		return model.UserLogin{}, handleErrorMysql(err)
	}
	defer result.Close()

	if result.Next() {
		var selectUser model.UserLogin
		showall := result.Scan(
			&selectUser.Firstname,
			&selectUser.Email,
		)
		if showall != nil {
			return model.UserLogin{}, handleErrorMysql(err)
		}
		fmt.Printf("repository select user login : %v\n", selectUser)
		return selectUser, nil

	} else {
		return model.UserLogin{}, handleErrorMysql(err)
	}
}

func handleErrorMysql(err error) error {
	switch err {
	case model.ErrInvalidConn:
		return errors.New("invalid connection")
	case model.ErrMalformPkt:
		return errors.New("malformed packet")
	case model.ErrNoTLS:
		return errors.New("TLS requested but server does not support TLS")
	case model.ErrCleartextPassword:
		return errors.New("this user requires clear text authentication. If you still want to use it, please add 'allowCleartextPasswords=1' to your DSN")
	case model.ErrNativePassword:
		return errors.New("this user requires mysql native password authentication.")
	default:
		return model.ErrNoRows
	}
}

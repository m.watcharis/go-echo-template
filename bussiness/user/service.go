package user

import (
	"watcharis/model"
)

type Service interface {
	GetUser() ([]model.UserResponse, error)
	AddUser(data model.UserRequest) (model.UserResponse, error)
	Login(data model.UserLogin) (res string, err error)
}

package services

import (
	"fmt"
	"watcharis/bussiness/user"
	"watcharis/model"
)

type service struct {
	repository user.Repository
}

func NewUserService(repository user.Repository) user.Service {
	return &service{
		repository: repository,
	}
}

func (r *service) GetUser() ([]model.UserResponse, error) {
	test := []model.UserResponse{}
	mond := model.UserResponse{
		Firstname: "test",
		Lastname:  "test",
		Email:     "test@test.com",
	}
	test = append(test, mond)
	return test, nil
}

func (r *service) AddUser(data model.UserRequest) (model.UserResponse, error) {
	repo, err := r.repository.AddUser(data)
	if err != nil {
		return model.UserResponse{}, err
	}
	fmt.Println("repo ==========>", repo)
	adduser := model.UserResponse{
		Firstname: data.Firstname,
		Lastname:  data.Lastname,
		Email:     data.Email,
	}
	return adduser, nil
}

func (r *service) Login(data model.UserLogin) (res string, err error) {
	repo, err := r.repository.Login(data)
	if err != nil {
		return "", err
	}
	fmt.Println("repo ========>", repo)
	return "Login success", err
}

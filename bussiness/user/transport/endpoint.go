package transport

import (
	"net/http"
	"watcharis/bussiness/user"
	"watcharis/model"

	"github.com/labstack/echo"
)

type Handler struct {
	service user.Service
}

func NewUserHandler(service user.Service) Handler {
	return Handler{
		service: service,
	}
}

//endpoint จะเรียก service
func (h Handler) GetUser(ctx echo.Context) (err error) {
	response, err := h.service.GetUser()
	if err != nil {
		return ctx.JSON(handlerErrorHttp(err), model.JsonResponse{Message: "Error boom", Status: "fail", Data: []string{}})
	}
	return ctx.JSON(http.StatusOK, model.JsonResponse{Message: "oh yeah", Status: "success", Data: response})
}

func (h Handler) AddUser(ctx echo.Context) (err error) {
	reqbodyuser := new(model.UserRequest)
	if err := ctx.Bind(reqbodyuser); err != nil {
		return ctx.JSON(handlerErrorHttp(err), model.JsonResponse{Message: "invalid data", Status: "fail", Data: []string{}})
	}
	response, err := h.service.AddUser(*reqbodyuser)
	if err != nil {
		return ctx.JSON(handlerErrorHttp(err), model.JsonResponse{Message: "Error boom", Status: "fail", Data: []string{}})
	}
	return ctx.JSONPretty(http.StatusOK, model.JsonResponse{Message: "AddUser success", Status: "success", Data: response}, " ")
}

func (h Handler) Login(ctx echo.Context) (err error) {
	reqbodyuser := new(model.UserLogin)
	if err := ctx.Bind(reqbodyuser); err != nil {
		return ctx.JSON(handlerErrorHttp(err), model.JsonResponse{Message: "invalid data", Status: "fail", Data: []string{}})
	}
	response, err := h.service.Login(*reqbodyuser)
	if err != nil {
		return ctx.JSON(handlerErrorHttp(err), model.JsonResponse{Message: "Error boom", Status: "fail", Data: []string{}})
	}
	return ctx.JSONPretty(http.StatusOK, model.JsonResponse{Message: "Login success", Status: "success", Data: response}, " ")
}

func handlerErrorHttp(err error) int {
	switch err {
	case model.ErrInternalServerError:
		return http.StatusInternalServerError
	case model.ErrNotFound:
		return http.StatusNotFound
	case model.ErrConflict:
		return http.StatusConflict
	case model.ErrBadParamInput:
		return http.StatusBadRequest
	default:
		return http.StatusInternalServerError
	}
}

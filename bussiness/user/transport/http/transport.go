package http

import (
	"watcharis/bussiness/user/transport"

	"github.com/labstack/echo"
)

type httpRoute struct {
	handler transport.Handler
}

// ประกาศ consturctor
func NewUserHttpRoute(handler transport.Handler) httpRoute {
	return httpRoute{
		handler: handler,
	}
}

//สร้าง path route ของ api โดยเรียก endpoint
func (h httpRoute) RouterPublic(e *echo.Group) {
	router := e.Group("/api/v1/user")
	router.GET("/getuser", h.handler.GetUser)
	router.POST("/adduser", h.handler.AddUser)
	router.POST("/login", h.handler.Login)
}

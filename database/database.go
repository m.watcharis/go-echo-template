package database

import (
	"database/sql"
	"fmt"
)

func Initialize(db *sql.DB) {
	_, table_users_err := db.Exec(`CREATE TABLE IF NOT EXISTS users (id int NOT NULL AUTO_INCREMENT PRIMARY KEY, firstname varchar(255) NOT NULL UNIQUE, lastname varchar(255) NOT NULL, email varchar(255) NOT NULL UNIQUE)`)
	if table_users_err != nil {
		fmt.Println(table_users_err)
	}

	_, table_foods_err := db.Exec(`CREATE TABLE IF NOT EXISTS foods (id int NOT NULL AUTO_INCREMENT PRIMARY KEY, food_name varchar(255) NOT NULL UNIQUE)`)
	if table_foods_err != nil {
		fmt.Println(table_foods_err)
	}

	_, table_cars_err := db.Exec(`CREATE TABLE IF NOT EXISTS cars (id int NOT NULL AUTO_INCREMENT PRIMARY KEY, color varchar(255) NOT NULL, gen varchar(255) NOT NULL, prices varchar(255) NOT NULL)`)
	if table_cars_err != nil {
		fmt.Println(table_cars_err)
	}
}

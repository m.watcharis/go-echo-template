package main

import (
	"database/sql"
	"fmt"
	"log"
	database "watcharis/database"

	"net/http"
	userRepository "watcharis/bussiness/user/repository"
	userService "watcharis/bussiness/user/services"
	userTransport "watcharis/bussiness/user/transport"
	userHttpRoute "watcharis/bussiness/user/transport/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gopkg.in/go-playground/validator.v9"
)

type UserResToServer []UserBody

type UserBody struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Email     string `json:"email"`
}

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

func main() {
	// fmt.Println("hello go")
	// fmt.Println(demoecho.Processuser())
	db, err := sql.Open("mysql", "admin:password@tcp(localhost:3333)/template-golang")

	// if there is an error opening the connection, handle it
	if err != nil {
		panic(err.Error())
	} else {
		fmt.Println("connect success")
	}
	database.Initialize(db)
	// defer the close till after the main function has finished
	// executing
	defer db.Close()

	e := echo.New()
	e.Validator = &CustomValidator{validator: validator.New()}

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})

	e.POST("/adduser", func(c echo.Context) (err error) {

		id := c.QueryParam("id")
		fmt.Println("id =======>", id)

		u := new(UserResToServer)
		if err := c.Bind(u); err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}

		for _, i := range *u {
			choses, err := db.Query(`SELECT users.firstname, users.email FROM users WHERE firstname=? AND email=?`, i.Firstname, i.Email)
			if err != nil {
				return echo.NewHTTPError(http.StatusBadRequest, err.Error())
			}

			if choses.Next() {
				var users UserBody
				err := choses.Scan(
					&users.Firstname,
					&users.Email,
				)

				if err != nil {
					log.Fatal(err)
				}

				fmt.Printf("%v\n", users)
				fmt.Println(users.Firstname)
			} else {

				fmt.Println("No users found")
			}

			insert, err := db.Prepare(`INSERT INTO users(firstname, lastname, email) VALUES ( ?, ?, ?)`)
			if err != nil {
				return echo.NewHTTPError(http.StatusBadRequest, err.Error())
			}
			defer insert.Close()
			_, err = insert.Exec(i.Firstname, i.Lastname, i.Email)
			if err != nil {
				return c.JSON(http.StatusBadRequest, "query insert user fail")
			}
		}

		show_all, err := db.Query(`SELECT * FROM users`)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		for show_all.Next() {
			var select_all UserBody
			result := show_all.Scan(
				&select_all.Firstname,
				&select_all.Lastname,
				&select_all.Email,
			)
			if result != nil {
				log.Fatal(result)
			}
			fmt.Printf("%v\n", select_all)
		}

		return c.JSONPretty(http.StatusOK, u, " ")
	})

	RouterPublic := e.Group("/publice")
	// repository
	userrepository := userRepository.NewUserRepository(db)
	// service
	userservice := userService.NewUserService(userrepository)
	// endpoint
	usertransport := userTransport.NewUserHandler(userservice)
	// transport
	userhttproute := userHttpRoute.NewUserHttpRoute(usertransport)

	userhttproute.RouterPublic(RouterPublic)

	e.Logger.Fatal(e.Start(":4545"))
}
